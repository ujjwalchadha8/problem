import { Page2Component } from './page2/page2.component';
import { Page1Component } from './page1/page1.component';
import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

@NgModule({
  imports: [RouterModule.forRoot([
    {
      path: '',
      redirectTo: 'page1',
      pathMatch: 'full'
    }, {
      path: 'page1',
      component: Page1Component
    }, {
      path: 'page2',
      component: Page2Component
    }
  ])
  ],
  exports: [RouterModule]
})

export class AppRoutingModule {}
